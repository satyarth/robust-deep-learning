import torch
from torch import nn as nn
from torch.nn import functional as F
from torch.autograd import Variable

class Flatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), -1)

class FC(nn.Module):
    def __init__(self, f_in, h1, h2, f_out):
        super(FC, self).__init__()
        
        self.h1 = nn.Linear(f_in, h1)
        self.h2 = nn.Linear(h1, h2)
        self.output = nn.Linear(h2, f_out)
        
        
    def forward(self, x):
        x = self.h1(x)
        x = F.relu(x)
        x = self.h2(x)
        x = F.relu(x)
        x = self.output(x)
        x = F.relu(x)
        return x

class ConvNet(nn.Module):
    def __init__(self, c_in=1, c1=50, c2=100):
        super(ConvNet, self).__init__()
        
        
        self.conv1 = nn.Conv2d(in_channels=c_in, out_channels=c1, kernel_size=3, padding=0)
        self.relu1 = nn.ReLU(inplace=True)
        self.mp1 = nn.MaxPool2d(2)
        
        self.conv2 = nn.Conv2d(in_channels=c1, out_channels=c2, kernel_size=3, padding=0)
        self.relu2 = nn.ReLU(inplace=True)
        self.mp2 = nn.MaxPool2d(2)
        
        self.av = nn.AvgPool2d(5)
        self.lin1 = nn.Linear(c2, 10)
        
    def forward(self, x):
        
        x=self.relu1(self.conv1(x))
        x=self.mp1(x)
        
        x=self.relu2(self.conv2(x))
        x=self.mp2(x)
        
        x=self.av(x)
        x=self.lin1(x.view(x.size(0),-1))
        
        return x
